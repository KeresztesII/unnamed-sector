#include "Drawables.h"

#ifndef MENUBUTTONS_H
#define MENUBUTTONS_H

class PlayButton: public Drawables
{
static std::string path;

public:
    PlayButton(SDL_Renderer*, Vec2D*);
    ~PlayButton();

    virtual void render ();
    virtual void animate ();
};

class HangarButton: public Drawables
{
static std::string path;

public:
    HangarButton(SDL_Renderer*, Vec2D*);
    ~HangarButton();

    virtual void render ();
    virtual void animate ();
};

class ProfileButton: public Drawables
{
static std::string path;

public:
    ProfileButton(SDL_Renderer*, Vec2D*);
    ~ProfileButton();

    virtual void render ();
    virtual void animate ();
};

class SettingsButton: public Drawables
{
static std::string path;

public:
    SettingsButton(SDL_Renderer*, Vec2D*);
    ~SettingsButton();

    virtual void render ();
    virtual void animate ();
};

class QuitButton: public Drawables
{
static std::string path;

public:
    QuitButton(SDL_Renderer*, Vec2D*);
    ~QuitButton();

    virtual void render ();
    virtual void animate ();
};

#endif