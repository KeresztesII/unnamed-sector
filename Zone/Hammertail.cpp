#include "Hammertail.h"

std::string Hammertail::path = "resources/images/F5-Scarab.png";

Hammertail::Hammertail(SDL_Renderer* renderer): Ship (renderer, path)
{
    initialize();
}

Hammertail::~Hammertail()
{
    delete center;
}

void Hammertail::initialize()
{
    center = new SDL_Point;
    center->x = 252;
    center->y = 575;
    angle = 0.0;
    rotationSpeed = 90;
}