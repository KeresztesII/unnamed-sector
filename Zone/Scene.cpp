#include "Scene.h"
#include "MainMenu.h"
#include "IngameMenu.h"
#include "Cursors.h"
#include "DynamicBackground.h"
#include "Hammertail.h"

int Scene::screenX = 1920;
int Scene::screenY = 1080;

int Scene::PLAYER_START_POS_X = screenX / 2;
int Scene::PLAYER_START_POS_Y = screenY / 2;

Uint32 Scene::startingTime = 0;
Uint32 Scene::elapsedTime = 0;

Scene::Scene(): runningMenu (true), runningGame (false), runningGameMenu (false), menu (NULL), currentPlayerShip (NULL)
{
    window = SDL_CreateWindow ("Zone", 0, 0, screenX, screenY, SDL_WINDOW_SHOWN);
    renderer = SDL_CreateRenderer (window, -1, SDL_RENDERER_ACCELERATED);
    SDL_ShowCursor (0);
}

Scene::~Scene()
{
    std::vector<Drawables*>::iterator itr = drawablesMenu.end();
    for (;itr != drawablesMenu.begin();)
    {
        --itr;
        delete *itr;
        itr = drawablesMenu.erase(itr);
    }

    std::vector<Drawables*>::iterator it = drawablesGame.end();
    for (;it != drawablesGame.begin();)
    {
        --it;
        delete *it;
        it = drawablesGame.erase(it);
    }

    SDL_DestroyRenderer (renderer);
    SDL_DestroyWindow (window);
    SDL_Quit();
}

void Scene::runMenu()
{
    menu = new MainMenu(renderer);
    cursor = new StandardCursor(renderer);

    drawablesMenu.push_back(menu);
    drawablesMenu.push_back(cursor);

    while (runningMenu) 
    {
        while(SDL_PollEvent(&sysEvent) != 0)
        {
            if (sysEvent.type == SDL_KEYDOWN && sysEvent.key.repeat == 0)
            {
                if (sysEvent.key.keysym.sym == SDLK_ESCAPE) runningMenu = false;
            }
            mouseInMenu();
        }

    if (runningGame)
        runGame();

        SDL_RenderClear ( renderer );

        for (int i = 0, size = drawablesMenu.size(); i < size; ++i)
            drawablesMenu[i]->animate();

        for (int i = 0, size = drawablesMenu.size(); i < size; ++i)
            drawablesMenu[i]->render();

        SDL_RenderPresent ( renderer );
    }
}

void Scene::runGame()
{
    initGame();

    while (runningGame) 
    {
        while(SDL_PollEvent(&sysEvent) != 0)
        {
            handleUserInput();
        }
        SDL_RenderClear ( renderer );
        getTimer();

        updateDynamicBackground();

        for (int i = 0, size = drawablesGame.size(); i < size; ++i)
        {
            drawablesGame[i]->animate();
        }

        for (int i = 0, size = drawablesGame.size(); i < size; ++i)
        {
            drawablesGame[i]->render();
            cursor->render();
        }
        resetTimer();
        SDL_RenderPresent ( renderer );
    }

//Popback so cursor, wich is the last element, is not deleted twice
    drawablesGame.pop_back();
}

void Scene::ingameMenu()
{
    runningGameMenu = true;

    gameMenu = new IngameMenu (renderer);

    while (runningGameMenu)
    {
        while (SDL_PollEvent (&sysEvent) != 0)
        {
            if (sysEvent.key.keysym.sym == SDLK_RETURN && sysEvent.key.repeat == 0) runningGameMenu = false;

            mouseInGameMenu();
        }
        SDL_RenderClear (renderer);

            gameMenu->render();
            cursor->animate();
            cursor->render();

        SDL_RenderPresent (renderer);
    }

    delete gameMenu;
}

void Scene::initGame()
{
    dynamicBackground = new DynamicBackground(renderer);
    drawablesGame.push_back(dynamicBackground);
    currentPlayerShip = new Hammertail(renderer);
    drawablesGame.push_back(currentPlayerShip);

//Has to be the last element
    drawablesGame.push_back(cursor);
}

void Scene::mouseInMenu()
{
//Check for play klick
    if (SDL_HasIntersection (&cursor->mousePos, &menu->buttons[0]->body) && sysEvent.type == SDL_MOUSEBUTTONDOWN)
        runningGame = true;

//Check for quit klick
    if (SDL_HasIntersection (&cursor->mousePos, &menu->buttons[4]->body) && sysEvent.type == SDL_MOUSEBUTTONDOWN)
        runningMenu = false;
}

void Scene::mouseInGameMenu()
{
    if (SDL_HasIntersection (&cursor->mousePos, &gameMenu->buttons[0]->body) && sysEvent.type == SDL_MOUSEBUTTONDOWN)
        runningGameMenu = false;

    if (SDL_HasIntersection (&cursor->mousePos, &gameMenu->buttons[2]->body) && sysEvent.type == SDL_MOUSEBUTTONDOWN)
    {
        runningGameMenu = false;
        runningGame = false;
    }
}

void Scene::handleUserInput()
{
    if (sysEvent.type == SDL_KEYDOWN && sysEvent.key.repeat == 0)
    {
        if (sysEvent.key.keysym.sym == SDLK_ESCAPE && sysEvent.key.repeat == 0) ingameMenu();

        currentPlayerShip->handleInputDOWN(sysEvent);
    }
    if (sysEvent.type == SDL_KEYUP && sysEvent.key.repeat == 0)
    {
        currentPlayerShip->handleInputUP(sysEvent);
    }
}

int Scene::getScreenX()
{
    return screenX;
}

int Scene::getScreenY()
{
    return screenY;
}

int Scene::getStartPosX()
{
    return PLAYER_START_POS_X;
}

int Scene::getStartPosY()
{
    return PLAYER_START_POS_Y;
}

void Scene::getTimer()
{
    elapsedTime = SDL_GetTicks() - startingTime;
}

void Scene::resetTimer()
{
    elapsedTime = 0;
    startingTime = SDL_GetTicks();
}

void Scene::updateDynamicBackground()
{
    resetBackground();

    dynamicBackground->setVelX (currentPlayerShip->getSpeed() * cos(currentPlayerShip->getAngle() * 0.0174532925) / 1000);
    dynamicBackground->setVelY (currentPlayerShip->getSpeed() * sin(currentPlayerShip->getAngle() * 0.0174532925) / 1000);

    dynamicBackground->body.x = dynamicBackground->getVelX();
    dynamicBackground->body.y = dynamicBackground->getVelY();
}

void Scene::resetBackground()
{
    if (dynamicBackground->body.x < 0)
        dynamicBackground->resetVelX(6000);

    if (dynamicBackground->body.x > 6000)
        dynamicBackground->resetVelX(0);

    if (dynamicBackground->body.y < 0)
        dynamicBackground->resetVelY(3000);

    if (dynamicBackground->body.y > 3000)
        dynamicBackground->resetVelY(0);
}