#include "Drawables.h"

#ifndef SHIP_H
#define SHIP_H

class Ship: public Drawables
{
bool forward, backward, left, right;

    void displaceShip();

protected:
//Alingment and speed

    SDL_Point* center;
    float speed;
    float rotationSpeed;
    double angle;
    float velX, velY;
    float tempPosX, tempPosY;


//PlayerCustom
    bool activeRotationSystems;

public:
    Ship(SDL_Renderer*, std::string);
    ~Ship();

    void handleInputDOWN(SDL_Event);
    void handleInputUP(SDL_Event);

    virtual void render ();
    virtual void animate ();

    float returnVelX();
    float returnVelY();
    double getAngle();
    float getSpeed();

};

#endif