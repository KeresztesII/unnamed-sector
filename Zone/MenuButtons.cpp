#include "MenuButtons.h"

std::string PlayButton::path = "resources/images/menuButtons/PlayButton.png";
std::string HangarButton::path = "resources/images/menuButtons/HangarButton.png";
std::string ProfileButton::path = "resources/images/menuButtons/ProfileButton.png";
std::string SettingsButton::path = "resources/images/menuButtons/SettingsButton.png";
std::string QuitButton::path = "resources/images/menuButtons/QuitButton.png";


//PLAY BUTTON
PlayButton::PlayButton(SDL_Renderer* renderer, Vec2D* pos)
{
    dRenderer = renderer;

    SDL_Surface* temp = IMG_Load (path.c_str());
    body.w = temp->w / 4;
    body.h = temp->h / 4;
    body.x = pos->x;
    body.y = pos->y;
    texture = SDL_CreateTextureFromSurface (renderer, temp);
    SDL_FreeSurface(temp);
}

void PlayButton::render()
{
    SDL_RenderCopy (dRenderer, texture, 0, &body);
}

void PlayButton::animate()
{

}

PlayButton::~PlayButton()
{
}

//HANGAR BUTTON
HangarButton::HangarButton(SDL_Renderer* renderer, Vec2D* pos)
{
    dRenderer = renderer;

    SDL_Surface* temp = IMG_Load (path.c_str());
    body.w = temp->w / 4;
    body.h = temp->h / 4;
    body.x = pos->x;
    body.y = pos->y;
    texture = SDL_CreateTextureFromSurface (renderer, temp);
    SDL_FreeSurface(temp);
}

void HangarButton::render()
{
    SDL_RenderCopy (dRenderer, texture, 0, &body);
}

void HangarButton::animate()
{

}

HangarButton::~HangarButton()
{
}

//PROFILE BUTTON
ProfileButton::ProfileButton(SDL_Renderer* renderer, Vec2D* pos)
{
    dRenderer = renderer;

    SDL_Surface* temp = IMG_Load (path.c_str());
    body.w = temp->w / 4;
    body.h = temp->h / 4;
    body.x = pos->x;
    body.y = pos->y;
    texture = SDL_CreateTextureFromSurface (renderer, temp);
    SDL_FreeSurface(temp);
}

void ProfileButton::render()
{
    SDL_RenderCopy (dRenderer, texture, 0, &body);
}

void ProfileButton::animate()
{

}

ProfileButton::~ProfileButton()
{
}

//SETTINGS BUTTON
SettingsButton::SettingsButton(SDL_Renderer* renderer, Vec2D* pos)
{
    dRenderer = renderer;

    SDL_Surface* temp = IMG_Load (path.c_str());
    body.w = temp->w / 4;
    body.h = temp->h / 4;
    body.x = pos->x;
    body.y = pos->y;
    texture = SDL_CreateTextureFromSurface (renderer, temp);
    SDL_FreeSurface(temp);
}

void SettingsButton::render()
{
    SDL_RenderCopy (dRenderer, texture, 0, &body);
}

void SettingsButton::animate()
{

}

SettingsButton::~SettingsButton()
{
}

//QUIT BUTTON
QuitButton::QuitButton(SDL_Renderer* renderer, Vec2D* pos)
{
    dRenderer = renderer;

    SDL_Surface* temp = IMG_Load (path.c_str());
    body.w = temp->w / 4;
    body.h = temp->h / 4;
    body.x = pos->x;
    body.y = pos->y;
    texture = SDL_CreateTextureFromSurface (renderer, temp);
    SDL_FreeSurface(temp);
}

void QuitButton::render()
{
    SDL_RenderCopy (dRenderer, texture, 0, &body);
}

void QuitButton::animate()
{

}

QuitButton::~QuitButton()
{
}