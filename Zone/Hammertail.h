#include "Ship.h"

#ifndef HAMMERTAIL_H
#define HAMMERTAIL_H

class Hammertail: public Ship
{
static std::string path;

void initialize();

public:
    Hammertail(SDL_Renderer*);
    ~Hammertail();
};

#endif