#include <iostream>
#include <cmath>

#ifndef VEC2D_H
#define VEC2D_H

struct Vec2D {
	float x;
	float y;

	Vec2D( );
	Vec2D(float, float);
	float length() const;
	Vec2D normalize() const;
};

Vec2D operator* (float a, Vec2D const& v); /** a*v */
Vec2D operator* (Vec2D const& v, float a); /** v*a */
Vec2D operator/ (Vec2D const& v, float a); /** v/a */

Vec2D operator+ (Vec2D const& v1, Vec2D const& v2); /** v1 + v2 */
Vec2D operator- (Vec2D const& v1, Vec2D const& v2); /** v1 - v2 */

Vec2D& operator+= (Vec2D& v1, Vec2D const& v2); /** v1 += v2 */
Vec2D& operator-= (Vec2D& v1, Vec2D const& v2); /** v1 -= v2 */

std::ostream& operator<<(std::ostream& os, Vec2D const& v); /** std::cout << v; */
/** [x, y] */

namespace coords
{
    Vec2D const o;
    Vec2D const i(1, 0);
    Vec2D const j(0, 1);
}

#endif