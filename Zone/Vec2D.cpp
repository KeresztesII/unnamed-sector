#include "Vec2D.h"


Vec2D::Vec2D( ): x(0.f), y(0.f) {
}

Vec2D::Vec2D(float x0, float y0): x(x0), y(y0) {
}

float Vec2D::length() const {
	return sqrt(x*x + y*y);
}

Vec2D operator* (float a, Vec2D const& v) {
	return Vec2D(a*v.x, a*v.y);
}

Vec2D operator* (Vec2D const& v, float a) {
	return a*v;
}

Vec2D operator/ (Vec2D const& v, float a) {
	return 1.f/a * v;
}

Vec2D Vec2D::normalize() const {
	return (*this)/this->length();
}


Vec2D operator+ (Vec2D const& v1, Vec2D const& v2) {
	return Vec2D(v1.x + v2.x, v1.y + v2.y);
}

Vec2D operator- (Vec2D const& v1, Vec2D const& v2) {
	return Vec2D(v1.x - v2.x, v1.y - v2.y);
}

Vec2D& operator+= (Vec2D& v1, Vec2D const& v2) {
	v1 = v1 + v2;
	return v1;
}

Vec2D& operator-= (Vec2D& v1, Vec2D const& v2) {
	v1 = v1 - v2;
	return v1;
}

std::ostream& operator<<(std::ostream& os, Vec2D const& v) {
	os << "[" << v.x << ", " << v.y << "]";
	return os;
}
