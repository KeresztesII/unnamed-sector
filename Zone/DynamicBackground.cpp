#include "DynamicBackground.h"
#include "Scene.h"

std::string DynamicBackground::path = "resources/images/Backgrounds/BigSpace.jpg";

DynamicBackground::DynamicBackground(SDL_Renderer* renderer): Background(renderer, path)
{
    body.w = Scene::getScreenX();
    body.h = Scene::getScreenY();
    body.x = 3000;
    body.y = 1500;

    velX = 0;
    velY = 0;
}

DynamicBackground::~DynamicBackground()
{

}

void DynamicBackground::setVelX(float rofl)
{
    velX += rofl;
}

void DynamicBackground::setVelY(float rofl)
{
    velY += rofl;
}

float DynamicBackground::getVelX()
{
    return velX;
}

float DynamicBackground::getVelY()
{
    return velY;
}

void DynamicBackground::resetVelX(int rofl)
{
    velX = rofl;
}

void DynamicBackground::resetVelY(int rofl)
{
    velY = rofl;
}
