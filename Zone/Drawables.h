#include <SDL.h>
#include <SDL_image.h>
#include <vector>
#include <string>
#include "Vec2D.h"

#ifndef DRAWABLES_H
#define DRAWABLES_H

class Ship;

class Drawables
{
protected:

    SDL_Texture* texture;
    SDL_Renderer* dRenderer;

public:
    Drawables();
    ~Drawables();

    virtual void render() = 0;
    virtual void animate() = 0;

    SDL_Rect body;
};

#endif