#include "Background.h"

#ifndef DYNAMICBACKGROUND_H
#define DYNAMICBACKGROUND_H

class DynamicBackground: public Background
{
static std::string path;

    float velX;
    float velY;

public:
    DynamicBackground(SDL_Renderer*);
    ~DynamicBackground();

    void setVelX(float);
    void setVelY(float);
    void resetVelX(int);
    void resetVelY(int);
    float getVelX();
    float getVelY();
};

#endif