#include "Drawables.h"

#ifndef BACKGROUND_H
#define BACKGROUND_H

class Ship;

class Background: public Drawables
{

public:
    Background(SDL_Renderer*, std::string);
    ~Background();

    virtual void render();
    virtual void animate ();
};

#endif