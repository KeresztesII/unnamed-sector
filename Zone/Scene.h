#include "Drawables.h"

#ifndef SCENE_H
#define SCENE_H

class MainMenu;
class DynamicBackground;
class CursorBase;
class Ship;
class Time;
class IngameMenu;

class Scene
{
    static Time timer;

//System essentials
    SDL_Window* window;
    SDL_Renderer* renderer;
    static int screenX;
    static int screenY;
    bool runningMenu;
    bool runningGame;
    bool runningGameMenu;
    SDL_Event sysEvent;

//Mouse essentials
    CursorBase* cursor;

    IngameMenu* gameMenu;
    MainMenu* menu;
    DynamicBackground* dynamicBackground;
    Ship* currentPlayerShip;

//Drawables type
    std::vector<Drawables*> drawablesMenu;
    std::vector<Drawables*> drawablesGame;
    std::vector<Drawables*> drawablesIngameMenu;

//System functions
    void mouseInMenu();
    void mouseInGameMenu();
    void initGame();
    void handleUserInput();
    void runGame ();
    void ingameMenu();

//DynamicBackgroundFunctions
    void updateDynamicBackground();
    void resetBackground();

//Player initialization
    static int PLAYER_START_POS_X;
    static int PLAYER_START_POS_Y;

public:
    Scene();
    ~Scene();

    static int getScreenX();
    static int getScreenY();
    static int getStartPosX();
    static int getStartPosY();

    void runMenu ();

//Timers
    static Uint32 elapsedTime;
    static Uint32 startingTime;

    static void getTimer();
    static void resetTimer();
};

#endif