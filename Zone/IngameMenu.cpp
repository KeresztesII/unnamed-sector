#include "IngameMenu.h"
#include "MenuButtons.h"

std::string IngameMenu::path = "resources/images/Backgrounds/PauseBackground.png";

IngameMenu::IngameMenu(SDL_Renderer* renderer): Menu (path, renderer)
{
    buttonAmount = 3;
    initButtons (renderer);
}


IngameMenu::~IngameMenu()
{
 //buttons not deleted !!!
}

void IngameMenu::initButtons(SDL_Renderer* renderer)
{
    int topDown = 250;

    for (int i = 0; i < buttonAmount; ++i)
    {
        buttonPos.push_back(new Vec2D);
    }

    for (int i = 0; i < buttonAmount; ++i)
    {
        buttonPos[i]->x = 500;
        buttonPos[i]->y = topDown;

        topDown += 100;
    }

    buttons.push_back(new PlayButton (renderer, buttonPos[0]));
    buttons.push_back(new SettingsButton (renderer, buttonPos[1]));
    buttons.push_back(new QuitButton (renderer, buttonPos[2]));
}