#include "Drawables.h"

#ifndef MENU_H
#define MENU_H

class Menu: public Drawables
{
protected:
    std::vector<Vec2D*> buttonPos;

public:
    Menu(std::string path, SDL_Renderer*);
    ~Menu();

    std::vector<Drawables*> buttons;
    virtual void render();
    virtual void animate();
};

#endif