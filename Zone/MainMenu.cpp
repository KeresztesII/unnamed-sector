#include "MainMenu.h"
#include "MenuButtons.h"

std::string MainMenu::path = "resources/images/Backgrounds/Background.jpg";

MainMenu::MainMenu (SDL_Renderer* renderer): Menu (path, renderer)
{
    buttonAmount = 5;
    initButtons(renderer);
}

MainMenu::~MainMenu ()
{
}

void MainMenu::initButtons(SDL_Renderer* renderer)
{
    int topDown = 400;

    for (int i = 0; i < buttonAmount; ++i)
        buttonPos.push_back(new Vec2D);

    for (int i = 0; i < buttonAmount; ++i)
    {
        buttonPos[i]->x = 100;
        buttonPos[i]->y = topDown;
        topDown += 100;
    }

    buttons.push_back(new PlayButton (renderer, buttonPos[0]));
    buttons.push_back(new HangarButton (renderer, buttonPos[1]));
    buttons.push_back(new ProfileButton (renderer, buttonPos[2]));
    buttons.push_back(new SettingsButton (renderer, buttonPos[3]));
    buttons.push_back(new QuitButton (renderer, buttonPos[4]));
}
