#include "Background.h"
#include "Scene.h"

Background::Background(SDL_Renderer* renderer, std::string path)
{
    dRenderer = renderer;

    SDL_Surface* temp = IMG_Load (path.c_str());
    texture = SDL_CreateTextureFromSurface(renderer, temp);
    SDL_FreeSurface (temp);
}


Background::~Background()
{
}

void Background::render()
{
    SDL_RenderCopy (dRenderer, texture, &body, 0);
}

void Background::animate()
{
}