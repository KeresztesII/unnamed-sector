#include "Menu.h"

Menu::Menu(std::string path, SDL_Renderer* renderer)
{
    dRenderer = renderer;

    SDL_Surface* temp = IMG_Load (path.c_str());
    texture = SDL_CreateTextureFromSurface (renderer, temp);
    SDL_FreeSurface (temp);
}

void Menu::render()
{
//Render Background
    SDL_RenderCopy(dRenderer, texture, 0, 0);

//Render Buttons
    for (int i = 0, size = buttons.size(); i < size; ++i)
        buttons[i]->render();
}

void Menu::animate()
{

}

Menu::~Menu()
{
    for (int i = 0, size = buttons.size(); i < size; ++i)
        delete buttons[i];

    for (int i = 0, size = buttonPos.size(); i < size; ++i)
        delete buttonPos[i];
}