#include "Cursors.h"

std::string StandardCursor::path = "resources/images/cursors/cursor.png";
std::string RedCursor::path = "resources/images/cursors/cursor_red.png";

StandardCursor::StandardCursor(SDL_Renderer* renderer): CursorBase (renderer, path)
{
}


StandardCursor::~StandardCursor()
{
}

RedCursor::RedCursor(SDL_Renderer* renderer): CursorBase (renderer, path)
{
}


RedCursor::~RedCursor()
{
}
