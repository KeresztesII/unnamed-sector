#ifndef INGAMEMENU_H
#define INGAMEMENU_H

#include "menu.h"

class IngameMenu: public Menu
{
    static std::string path;

    int buttonAmount;
    void initButtons(SDL_Renderer*);

public:
    IngameMenu(SDL_Renderer*);
    ~IngameMenu();
};

#endif