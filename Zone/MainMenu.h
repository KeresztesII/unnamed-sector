#include "Menu.h"

#ifndef MAINMENU_H
#define MAINMENU_H

class MainMenu: public Menu
{
    static std::string path;

    int buttonAmount;
    void initButtons(SDL_Renderer*);

public:
    MainMenu(SDL_Renderer*);
    ~MainMenu();
};

#endif