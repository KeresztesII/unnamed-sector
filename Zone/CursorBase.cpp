#include "CursorBase.h"


CursorBase::CursorBase(SDL_Renderer* renderer, std::string path)
{
    dRenderer = renderer;

    SDL_Surface* temp = IMG_Load (path.c_str());
    texture = SDL_CreateTextureFromSurface (renderer, temp);
    body.w = temp->w;
    body.h = temp->h;
    body.x = 0;
    body.y = 0;
    SDL_FreeSurface (temp);

    mousePos.w = 2;
    mousePos.h = 2;
}

void CursorBase::render()
{
//Render Cursor
    SDL_RenderCopy(dRenderer, texture, 0, &body);
}

void CursorBase::animate ()
{
//Getting mouse position
    SDL_GetMouseState( &body.x, &body.y);

    mousePos.x = body.x;
    mousePos.y = body.y;
}

CursorBase::~CursorBase()
{
}
