#include "Ship.h"
#include "Scene.h"
#include <cmath>

Ship::Ship(SDL_Renderer* renderer, std::string path): center (NULL), angle (0.0), velX (0), velY(0), tempPosX (Scene::getStartPosX()), tempPosY (Scene::getStartPosY()), speed (0), forward (false), backward (false), left (false), right(false), activeRotationSystems (true)
{
    dRenderer = renderer;

    SDL_Surface* temp = IMG_Load (path.c_str());
    body.w = temp->w / 4;
    body.h = temp->h / 4;
    body.x = Scene::getStartPosX();
    body.y = Scene::getStartPosY();
    texture = SDL_CreateTextureFromSurface(renderer, temp);
    SDL_FreeSurface (temp);
}

Ship::~Ship()
{
    if (center != NULL)
    {
        delete center;
        center = NULL;
    }
}

void Ship::handleInputDOWN(SDL_Event sysEvent)
{
    if (sysEvent.key.keysym.sym == SDLK_w)
    {
        forward = true;
    }

    if (sysEvent.key.keysym.sym == SDLK_s)
    {
        backward = true;
    }

    if (sysEvent.key.keysym.sym == SDLK_a)
    {
        left = true;
    }

    if (sysEvent.key.keysym.sym == SDLK_d)
    {
        right = true;
    }
}

void Ship::handleInputUP(SDL_Event sysEvent)
{
    if (sysEvent.key.keysym.sym == SDLK_w)
    {
        forward = false;
    }

    if (sysEvent.key.keysym.sym == SDLK_s)
    {
        backward = false;
    }

    if (sysEvent.key.keysym.sym == SDLK_a)
    {
        left = false;
    }

    if (sysEvent.key.keysym.sym == SDLK_d)
    {
        right = false;
    }
}


void Ship::animate()
{
    if (activeRotationSystems)
    {
        if (left == true)
            angle -= rotationSpeed * (Scene::elapsedTime / 1000.f);

        if (right == true)
            angle += rotationSpeed * (Scene::elapsedTime / 1000.f);

        if (forward == true)
        {
            speed += 100 * (Scene::elapsedTime / 1000.f);
        }
        if (backward == true)
        {
            speed -= 100 * (Scene::elapsedTime / 1000.f);
        }

    //displaceShip();

        velX += (speed * cos(angle * (M_PI / 180))) * (Scene::elapsedTime / 1000.f);
        velY += (speed * sin(angle * (M_PI / 180))) * (Scene::elapsedTime / 1000.f);

    }/*
    else
    {
        if (left == true)
            angle -= rotationSpeed * Scene::elapsedTime / 1000.f;

        if (right == true)
            angle += rotationSpeed * Scene::elapsedTime / 1000.f;

        if (forward == true)
        {
            velX += (1000 * cos(angle * (M_PI / 180))) * Scene::elapsedTime / 1000.f;
            velY += (1000 * sin(angle * (M_PI / 180))) * Scene::elapsedTime / 1000.f;
        }
        if (backward == true)
        {
            acceleration -= 100* Scene::elapsedTime / 1000.f;
        }

        tempPosX += velX * Scene::elapsedTime / 1000.f;
        tempPosY += velY * Scene::elapsedTime / 1000.f;
        body.x = tempPosX;
        body.y = tempPosY;
    }*/
}

void Ship::render ()
{
    SDL_RenderCopyEx (dRenderer, texture, 0, &body, angle, NULL, SDL_FLIP_NONE);
}

float Ship::returnVelX()
{
    return velX;
}

float Ship::returnVelY()
{
    return velY;
}

float Ship::getSpeed()
{
    return speed;
}

double Ship::getAngle()
{
    return angle;
}

void Ship::displaceShip()
{
        body.x = Scene::getStartPosX() - (speed * cos(angle * (M_PI / 180)));
        body.y = Scene::getStartPosY() - (speed * sin(angle * (M_PI / 180)));

    if (body.x > (Scene::getScreenX() - (100 + body.w) ))
        body.x = (Scene::getScreenX() - (100 + body.w) );

    if (body.x < 100)
        body.x = 100;

    if (body.y > (Scene::getScreenY() - (100 + body.h) ))
        body.y = (Scene::getScreenY() - (100 + body.h) );

    if (body.y < 100)
        body.y = 100;
}