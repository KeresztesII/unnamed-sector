#include "CursorBase.h"

#ifndef CURSORS_H
#define CURSORS_H

class StandardCursor: public CursorBase
{
static std::string path;

public:
    StandardCursor(SDL_Renderer*);
    ~StandardCursor();
};

class RedCursor: public CursorBase
{
static std::string path;

public:
    RedCursor(SDL_Renderer*);
    ~RedCursor();
};

#endif