#include "Drawables.h"

#ifndef CURSORBASE_H
#define CURSORBASE_H

class CursorBase: public Drawables
{

public:
    CursorBase(SDL_Renderer*, std::string);
    ~CursorBase();

    SDL_Rect mousePos;

    virtual void render();
    virtual void animate ();
};

#endif